# Plattform & Service

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

- [ ] [YubiKey](https://yubikey.ch/de/)
- [ ] [Bitwarden - Open Source Password Management for You and Your Business](https://bitwarden.com/)
- [ ] [EuroDNS](https://my.eurodns.com/de/das/search/)
- [ ] [DNS Made Easy](https://auth.dnsmadeeasy.com/)
- [ ] [Mailbox.org](https://mailbox.org/de/)
- [ ] [Timme Hosting](https://timmehosting.de/)
  - [Scale Server](https://timmehosting.de/scaleserver)
- [ ] [Raidboxes](https://dashboard.raidboxes.de/boxes)
- [ ] [Xentral](https://xentral.com/de/)
  - [Einrichtung WooCommerce](https://community.xentral.com/hc/de/articles/360016761119-Woocommerce)

**Optional**
  - [/e/Cloud](https://ecloud.global/)
  - [Wasabi](https://wasabi.com/)

### Produktivität
- [Muster-Vorlage.ch](https://muster-vorlage.ch/#KMU)
- [StackEdit](https://stackedit.io/app#)
- [Public playlist: filmmusic.io](https://filmmusic.io/playlist/2260/nerer)
- [Videomaker Simpleshow](https://videomaker.simpleshow.com/de/)
- [Kimai – Time-tracking](https://www.kimai.cloud/)

## Ecommerce Plattform

dhdhdh

### Potentielle Händler

- [**THINK EARTHY**](https://www.thinkearthy.ch/)
  - [Plastikfreie Produkte](https://wemakeit.com/projects/plastikfreie-produkte/show/backers)
- [**MyNeons**](https://wemakeit.com/users/myneons)
- [**Die Faire Computermaus**](https://www.nager-it.de/): Für menschenwürdige Arbeitsbedingungen
in den Fabriken der Computerindustrie.
  - [Fair IT yourself](https://fair-it-yourself.de/): Fair IT yourself hat das Ziel das Thema Faire Elektronik zu verbreiten und Projekte und Unternehmen bei der Produktion nachhaltiger und fairer Elektronik zu unterstützen.



### Lokale Unternehmen

- [**Stadtbekannt**](https://www.stadtbekannt.online): DIE GESAMTE DIGITALE PRÄSENZ IHRES UNTERNEHMENS IN NUR EINEM TOOL.
  - [**SOgenda**](https://www.sogenda.ch/de): SOgenda, so heisst die neue Plattform der GAW. In der SOgenda findet man aktuelle Infos sowie einen umfassenden regionalen Veranstaltungskalender. Die GAW arbeitet dabei mit Gemeinden, kulturellen Institutionen, Solothurn Tourismus Gewerbe und Vereinen zusammen.

#### Kundenaquirierung
- [**Sonect**](https://sonect.net/ch-de/shops-ch-de/): Neuer Service, neue Kunden: Holen Sie sich jetzt Sonect in den Laden und geben Sie Kunden die Möglichkeit, Bargeld direkt an Ihrer Kasse zu beziehen.
- [**Bity ATM**](https://bity.com/de/products/crypto-atms/distributors): Geldautomaten für Bitcoin und andere Kryptowährungen geben dir die Möglichkeit, Bitcoin und Ether sofort vor Ort zu kaufen und verkaufen. Einfach, schnell, sicher, geringe Gebühren. In der Schweiz verwaltet und betrieben.
- [**To Good To Go**](https://toogoodtogo.ch/de-ch/business): Unsere Mission ist es, euch alle zu inspirieren und dazu zu bewegen, euch gegen Food Waste einzusetzen. Und es ist Zeit, Worte in Taten umzusetzen! Wir werden alles dafür geben, eine globale Bewegung gegen Food Waste aufzubauen.
    - [_Erst essen, was gut ist! – eine App hilft_](https://www.zeitpunkt.ch/index.php/erst-essen-was-gut-ist-eine-app-hilft)


